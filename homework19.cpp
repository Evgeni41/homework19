﻿#include <iostream>

class Animal
{
public:
    void virtual voice() = 0;
  
};

class Dog : public Animal
{
public:
    void voice() override
    {
        std::cout << "Woof!" << std::endl;
    }
};

class Cat : public Animal
{
public:
    void voice() override
    {
        std::cout << "Meow!" << std::endl;
    }
    
};

class Duck : public Animal
{
public:
    void voice() override
    {
        std::cout << "Quack!" << std::endl;
    }
};
int main()
{
    Animal* voices[4];
    voices[0] = new Cat();
    voices[1] = new Dog();
    voices[2] = new Duck();
    voices[3] = new Cat();
    for (auto elements : voices)
    {
        elements->voice();
        delete elements;
    }
    
    
    
}


